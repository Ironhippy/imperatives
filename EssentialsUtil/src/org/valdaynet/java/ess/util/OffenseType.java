package org.valdaynet.java.ess.util;

import java.util.List;

import com.massivecraft.massivecore.util.MUtil;
import com.massivecraft.massivecore.util.Txt;

public enum OffenseType {
	
	SPAM(Txt.parse("<rose>You have been banned for spam")),
	OFFENSIVE_BEHAVIOUR(Txt.parse("<rose>You have been banned for offensive behaviour")),
	HACKING(Txt.parse("<rose>You have been caught hacking")),
	OTHER(Txt.parse("<rose>You have been banned for an undefined reason")),
	DISRUPTIVE(Txt.parse("<rose>You have been disruptive towards other players"));
	
	private String Mainode;
	
	private OffenseType(String node)
	{
		this.Mainode = node;
	}
	
	public String getMessage(String info)
	{
		return this.Mainode+": "+Txt.parse("<i>"+info);
	}
	
	public String toOrdinalString()
	{
		if (this==SPAM)
		{
			return "spamming";
		}
		else if (this==OFFENSIVE_BEHAVIOUR)
		{
			return "offensive";
		}
		else if (this==HACKING)
		{
			return "hacking";
		}
		else if (this==OTHER)
		{
			return "other";
		}
		else if (this==DISRUPTIVE)
		{
			return "disruptive";
		}
		return null;
	}
	
	public boolean exists()
	{
		if (getTypes().contains(this.toOrdinalString()))
		{
			return true;
		}
		return false;
	}
	
	public static List<String> getTypes()
	{
		return MUtil.list("spamming", "offensive", "hacking", "other", "disruptive");
	}
}
