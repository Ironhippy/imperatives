package org.valdaynet.java.ess.util;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.valdaynet.java.ess.entity.MPlayer;

import com.massivecraft.massivecore.ps.PS;
import com.massivecraft.massivecore.util.Txt;

public class Lyncstone {
	
	private ItemStack lyn = null;
	private ItemMeta meta = null;
	
	public Lyncstone() {}
	private static Lyncstone lync = new Lyncstone();
	public static Lyncstone get() { return lync; }
	
	private ItemStack obtainStack() { lyn = new ItemStack(Material.NETHER_STAR, 1); return lyn; }
	private ItemMeta obtainMeta() { meta = lyn.getItemMeta(); meta.setDisplayName(Txt.parse("<lime>Lynstone")); lyn.setItemMeta(meta); return meta; }
	public ItemStack toItem() { lyn = this.obtainStack(); lyn.setItemMeta(this.obtainMeta()); return lyn; }
	
	public void activate(MPlayer mpl)
	{
		PS ps = mpl.getLyncLocation();
		mpl.getPlayer().teleport(ps.asBukkitLocation());
	}
}
