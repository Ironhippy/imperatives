package org.valdaynet.java.ess.util;

import org.bukkit.Bukkit;
import org.valdaynet.java.ess.entity.MConf;

public class DescribeFunctionality {
	
	private static DescribeFunctionality desc = new DescribeFunctionality();
	public static DescribeFunctionality get() { return desc; }
	
	public boolean checkFactions()
	{
		boolean isFactionsEnabled = isPluginEnabled("Factions");
		
		if (! isFactionsEnabled) return false;
		
		MConf.get().setFactionsCompliant();
		// TODO Write Factions Functionality and Integration
		
		return true;
	}
	
	public boolean checkVault()
	{
		boolean isVaultEnabled = isPluginEnabled("Vault");
		
		if (! isVaultEnabled) return false;
		
		MConf.get().setVaultCompliant();
		// TODO Write Vault Functionality and Integration
		
		return true;
	}
	
	public boolean checkHerochat()
	{
		boolean isHerochatEnabled = isPluginEnabled("HeroChat");
		
		if (! isHerochatEnabled) return false;
		
		MConf.get().setHerochatCompliant();
		// TODO Write HeroChat Functionality and Integration
		
		return true;
	}
	
	public boolean checkVampire()
	{
		boolean isVampireEnabled = isPluginEnabled("Vampire");
		
		if (! isVampireEnabled) return false;
		
		MConf.get().setVampireCompliant();
		// TODO Write Vampire Functionality and Integration
		
		return true;
	}
	
	private boolean isPluginEnabled(String plugin)
	{
		if (Bukkit.getPluginManager().getPlugin(plugin)!=null)
		{
			return true;
		}
		return false;
	}
}
