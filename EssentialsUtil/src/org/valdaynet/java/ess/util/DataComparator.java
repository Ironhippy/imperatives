package org.valdaynet.java.ess.util;

import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.valdaynet.java.ess.entity.MConf;
import org.valdaynet.java.ess.entity.MPlayer;

import com.dthielke.herochat.Chatter;
import com.dthielke.herochat.Herochat;
import com.massivecraft.massivecore.util.Txt;

public class DataComparator {
	
	private static DataComparator data = new DataComparator();
	public static DataComparator get() { return data; }
	
	public void getSeenCommandRegular(CommandSender sender, MPlayer mplayer)
	{
		
		String line = Txt.parse("<orange>_______.[ <lime>Player "+mplayer.getName()+" <orange>]._______\n");
		line = line+Txt.parse("<i>Player: <aqua>"+mplayer.getName()+"\n"+
				              "<i>Unique ID: <aqua>"+mplayer.getId());
		if (mplayer.isBanned())
		{
			line = line+Txt.parse("<i>Banned: <rose>Yes\n");
		}
		else if (!mplayer.isBanned())
		{
			line = line+Txt.parse("<i>Banned: <lime>No\n");
		}
		if (MConf.get().isVaultOverriden() && MConf.get().canSeeMoney())
		{
			line = line+Txt.parse("\n<i>Balance: <aqua>"+mplayer.getEconomyBalance());
		}
		
		if (MConf.get().isFactionsCompliant() && MConf.get().canSeeFaction())
		{
			line = line+Txt.parse("\n<i>Faction: <lime>"+mplayer.hasFactionFaction());
		}
		
		if (MConf.get().isVampireCompliant())
		{
			if (MConf.get().canSeeInfected())
			{
				String bool = null;
				if (mplayer.isVampireInfected())
				{
					bool = Txt.parse("<rose><italic>This player is infected");
				}
				else {
					bool = Txt.parse("<lime><italic>This player is not infected");
				}
				line = line+Txt.parse("\n<i>Infected with Vampirism: "+bool);
			}
			if (MConf.get().canSeeVampire())
			{
				String bool = null;
				if (mplayer.isVampireVampire())
				{
					bool = Txt.parse("<rose><italic>This player is a bloodthirsty vampire.");
				}
				else {
					bool = Txt.parse("<lime><italic>This player is perfectly healthy.");
				}
				line = line+Txt.parse("\n<i>Vampire: "+bool);
			}
		}
		
		if (MConf.get().isHerochatCompliant() && MConf.get().canSeeChannel())
		{
			Chatter chat = Herochat.getChatterManager().getChatter(mplayer.getPlayer());
			line = line+Txt.parse("\n<i>Chat Channel: <aqua>"+chat.getName());
		}
		sender.sendMessage(line);
	}
	
	public void getSeenCommandAdmin(CommandSender sender, MPlayer mplayer)
	{
		Location l = mplayer.getLocation().asBukkitLocation();
		String location = Txt.parse("<aqua>"+l.getWorld().getName()+
				                    "<i>, <aqua>"+l.getBlockX()+
				                    "<i>, <aqua>"+l.getBlockY()+
				                    "<i>, <aqua>"+l.getBlockZ());
		
		
		String line = Txt.parse("<orange>_______.[ <lime>Player "+mplayer.getName()+" <orange>]._______\n");
		line = line+Txt.parse("<i>Player: <aqua>"+mplayer.getName()+"\n"
				             +"<i>Unique ID: <aqua>"+mplayer.getId()+"\n"
				             +"<i>Location: "+location+"\n");
		if (mplayer.isBanned())
		{
			line = line+Txt.parse("<i>Banned: <rose>Yes<i>, for "+mplayer.getBanMessage()+".\n");
		}
		else if (!mplayer.isBanned())
		{
			line = line+Txt.parse("<i>Banned: <lime>No<i>, this player has <italic>never<i> broken our rules.\n");
		}
		if (mplayer.getUniverse()==null)
		{
			line = line+Txt.parse("<i>Universe: <silver><italic>It appears this player is in the <rose><strong>Null Void<silver><italic>.");
		}
		else if (mplayer.getUniverse()!=null)
		{
			line = line+Txt.parse("<i>Universe: <italic>"+mplayer.getUniverse());
		}
		if (MConf.get().isVaultOverriden() && MConf.get().canSeeMoney())
		{
			line = line+Txt.parse("\n<i>Balance: <aqua>"+mplayer.getEconomyBalance());
		}
		
		if (MConf.get().isFactionsCompliant() && MConf.get().canSeeFaction())
		{
			com.massivecraft.factions.entity.MPlayer m = com.massivecraft.factions.entity.MPlayer.get(sender);
			line = line+Txt.parse("\n<i>Faction: "+com.massivecraft.factions.entity.MPlayer.get(mplayer).getFaction().getName(m));
		}
		
		if (MConf.get().isVampireCompliant())
		{
			if (MConf.get().canSeeInfected())
			{
				String bool = null;
				if (mplayer.isVampireInfected())
				{
					bool = Txt.parse("<rose><italic>This player is infected");
				}
				else {
					bool = Txt.parse("<lime><italic>This player is not infected");
				}
				line = line+Txt.parse("\n<i>Infected with Vampirism: "+bool);
			}
			if (MConf.get().canSeeVampire())
			{
				String bool = null;
				if (mplayer.isVampireVampire())
				{
					bool = Txt.parse("<rose><italic>This player is a bloodthirsty vampire.");
				}
				else {
					bool = Txt.parse("<lime><italic>This player is perfectly healthy.");
				}
				line = line+Txt.parse("\n<i>Vampire: "+bool);
			}
		}
		
		if (MConf.get().isHerochatCompliant() && MConf.get().canSeeChannel())
		{
			Chatter chat = Herochat.getChatterManager().getChatter(mplayer.getPlayer());
			line = line+Txt.parse("\n<i>Chat Channel: <aqua>"+chat.getName());
		}
		sender.sendMessage(line);
	}
}
