package org.valdaynet.java.ess;

import org.bukkit.Bukkit;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.valdaynet.java.ess.cmd.BanCmd;
import org.valdaynet.java.ess.cmd.PardonCmd;
import org.valdaynet.java.ess.cmd.Seen;
import org.valdaynet.java.ess.entity.MConfColl;
import org.valdaynet.java.ess.entity.MPlayerColl;
import org.valdaynet.java.ess.event.EventRegister;
import org.valdaynet.java.ess.util.DescribeFunctionality;

import com.massivecraft.massivecore.MassivePlugin;
import com.massivecraft.massivecore.util.Txt;

import net.milkbowl.vault.economy.Economy;

public class EssentialUtil extends MassivePlugin {
	
	private static EssentialUtil i;
	public static EssentialUtil get() { return i; }
	public EssentialUtil() { EssentialUtil.i = this; }
	
	@Override
	public void onEnable()
	{
		if (!preEnable()) return;
		
		MConfColl.get().init();
		
		DescribeFunctionality desc = DescribeFunctionality.get();
		
		desc.checkFactions();
		desc.checkVault();
		desc.checkHerochat();
		desc.checkVampire();
		
		String version = this.getDescription().getVersion();
		
		Bukkit.getConsoleSender().sendMessage(Txt.parse("<teal>[<aqua>Imperitives "+version+"<teal>] <i>Beginning Initialization MPlayer Collection..."));
		MPlayerColl.get().init();
		
		BanCmd ban = new BanCmd();
		ban.register(this);
		
		PardonCmd pardon = new PardonCmd();
		pardon.register(this);
		
		Seen seen = new Seen();
		seen.register(this);
		
		EventRegister.get().listen();
		
		postEnable();
	}
	
	public Economy getEconomy()
	{
		RegisteredServiceProvider<Economy> registeredServiceProvider = Bukkit.getServicesManager().getRegistration(Economy.class);
		if (registeredServiceProvider == null) return null;
		return registeredServiceProvider.getProvider();
	}
}
