package org.valdaynet.java.ess.cmd;

import java.util.List;

import org.valdaynet.java.ess.Perm;
import org.valdaynet.java.ess.cmd.ar.ARMPlayer;
import org.valdaynet.java.ess.cmd.ar.AROffense;
import org.valdaynet.java.ess.entity.MPlayer;
import org.valdaynet.java.ess.util.OffenseType;

import com.massivecraft.massivecore.MassiveException;
import com.massivecraft.massivecore.cmd.MassiveCommand;
import com.massivecraft.massivecore.cmd.arg.ARString;
import com.massivecraft.massivecore.cmd.req.ReqHasPerm;
import com.massivecraft.massivecore.util.MUtil;
import com.massivecraft.massivecore.util.Txt;

public class BanCmd extends MassiveCommand {
	
	public BanCmd()
	{
		this.addArg(ARMPlayer.get(), "player");
		this.addArg(AROffense.get(), "offense type");
		this.addArg(ARString.get(), "additional details", true);
		
		this.addRequirements(ReqHasPerm.get(Perm.BAN_PLAYER.getNode()));
	}
	
	@Override
	public void perform() throws MassiveException {
		
		MPlayer mplayer = this.readArg();
		OffenseType type = this.readArg();
		String kickReason = this.readArg();
		
		if (mplayer.isBanned()) { sendMessage(Txt.parse("<rose>This player has already been banned. <i>Use /seen to check player data first.")); return;}
		
		if (!type.exists()) { sendMessage(Txt.parse("<rose>Offense Type <italic>\""+type.toOrdinalString()+"\"<rose> does not exist. <i>Press <TAB> after entering the player's name.")); return; }
		
		mplayer.setBanned(true, Txt.parse(kickReason));
		
		if (mplayer.isOnline()) { mplayer.getPlayer().kickPlayer(type.getMessage(kickReason)); }
	}
	
	@Override
	public List<String> getAliases()
	{
		return MUtil.list("ban");
	}
}
