package org.valdaynet.java.ess.cmd.event;

import com.massivecraft.massivecore.cmd.MassiveCommand;
import com.massivecraft.massivecore.cmd.VisibilityMode;

public class EventCommand extends MassiveCommand {
	
	public EventCommand()
	{
		this.setVisibilityMode(VisibilityMode.SECRET);
	}

}
