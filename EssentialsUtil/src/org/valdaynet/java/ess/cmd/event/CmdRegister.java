package org.valdaynet.java.ess.cmd.event;

import org.valdaynet.java.ess.Perm;

import com.massivecraft.massivecore.MassiveException;
import com.massivecraft.massivecore.cmd.arg.ARString;
import com.massivecraft.massivecore.cmd.req.ReqHasPerm;

public class CmdRegister extends EventCommand {
	
	public CmdRegister()
	{
		this.addAliases("register");
		
		this.desc = "register a new event";
		
		this.addArg(ARString.get(), "event");
		
		this.addRequirements(ReqHasPerm.get(Perm.EVENT_REGISTER.getNode()));
	}
	
	@Override
	public void perform() throws MassiveException {
		
	}
}
