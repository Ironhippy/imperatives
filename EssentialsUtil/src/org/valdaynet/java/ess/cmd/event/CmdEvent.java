package org.valdaynet.java.ess.cmd.event;

import java.util.List;

import com.massivecraft.massivecore.util.MUtil;

public class CmdEvent extends EventCommand {
	
	public CmdEvent()
	{
		
	}
	
	@Override
	public List<String> getAliases()
	{
		return MUtil.list("event");
	}
}
