package org.valdaynet.java.ess.cmd;

import java.util.List;

import org.valdaynet.java.ess.Perm;
import org.valdaynet.java.ess.cmd.ar.ARMPlayer;
import org.valdaynet.java.ess.entity.MPlayer;

import com.massivecraft.massivecore.MassiveException;
import com.massivecraft.massivecore.cmd.MassiveCommand;
import com.massivecraft.massivecore.cmd.req.ReqHasPerm;
import com.massivecraft.massivecore.util.MUtil;
import com.massivecraft.massivecore.util.Txt;

public class PardonCmd extends MassiveCommand {
	
	public PardonCmd()
	{
		this.addArg(ARMPlayer.get(), "player");
		
		this.addRequirements(ReqHasPerm.get(Perm.PARDON_PLAYER.getNode()));
	}
	
	@Override
	public void perform() throws MassiveException 
	{
		MPlayer mplayer = this.readArg();
		
		if (!mplayer.isBanned()) { sendMessage(Txt.parse("<rose>This player is not banned. <i>Use /seen to check player data first.")); return; }
		
		mplayer.setBanned(false, mplayer.getBanMessage());
		
		sendMessage(Txt.parse("<i>The player <italic>"+mplayer.getName()+" <i>has been unbanned."));
	}
	
	@Override
	public List<String> getAliases()
	{
		return MUtil.list("pardon");
	}

}
