package org.valdaynet.java.ess.cmd.ar;

import java.util.Collection;

import org.bukkit.command.CommandSender;
import org.valdaynet.java.ess.util.OffenseType;

import com.massivecraft.massivecore.MassiveException;
import com.massivecraft.massivecore.cmd.arg.ARAbstract;

public class AROffense extends ARAbstract<OffenseType> {
	
	private static AROffense off = new AROffense();
	public static AROffense get() { return off; }

	@Override
	public OffenseType read(String arg, CommandSender sender) throws MassiveException {
		OffenseType type = null;
		
		if (arg.equalsIgnoreCase("hacking")) type=OffenseType.HACKING;
		if (arg.equalsIgnoreCase("spamming")) type=OffenseType.SPAM;
		if (arg.equalsIgnoreCase("offensive")) type=OffenseType.OFFENSIVE_BEHAVIOUR;
		if (arg.equalsIgnoreCase("disruptive")) type=OffenseType.DISRUPTIVE;
		if (arg.equalsIgnoreCase("other")) type=OffenseType.OTHER;
		
		return type;
	}

	@Override
	public Collection<String> getTabList(CommandSender sender, String arg) {
		return OffenseType.getTypes();
	}

}
