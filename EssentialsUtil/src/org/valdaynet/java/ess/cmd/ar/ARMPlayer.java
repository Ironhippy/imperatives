package org.valdaynet.java.ess.cmd.ar;

import org.valdaynet.java.ess.entity.MPlayer;
import org.valdaynet.java.ess.entity.MPlayerColl;

import com.massivecraft.massivecore.cmd.arg.AR;

public class ARMPlayer {
	
	public static AR<MPlayer> get()
	{
		return MPlayerColl.get().getAREntity();
	}
	
}
