package org.valdaynet.java.ess.cmd;

import java.util.List;

import org.valdaynet.java.ess.Perm;
import org.valdaynet.java.ess.cmd.ar.ARMPlayer;
import org.valdaynet.java.ess.entity.MPlayer;
import org.valdaynet.java.ess.util.DataComparator;

import com.massivecraft.massivecore.MassiveException;
import com.massivecraft.massivecore.cmd.MassiveCommand;
import com.massivecraft.massivecore.cmd.VisibilityMode;
import com.massivecraft.massivecore.util.MUtil;
import com.massivecraft.massivecore.util.Txt;

public class Seen extends MassiveCommand {
	
	public Seen()
	{
		this.setVisibilityMode(VisibilityMode.VISIBLE);
		
		this.addArg(ARMPlayer.get(), "player");
	}
	
	@Override
	public void perform() throws MassiveException {
		
		MPlayer mplayer = this.readArg(MPlayer.get(sender));
		
		if (mplayer == null) return;
		if (mplayer.isConsole()) {sendMessage(Txt.parse("<rose>You cannot access the console's data. <i>Try doing it's job for a day.")); return;}
		
		if (sender.hasPermission(Perm.SEEN_ADMIN.getNode()))
		{
		    DataComparator.get().getSeenCommandAdmin(sender, mplayer);
		}
		else if (sender.hasPermission(Perm.SEEN.getNode()))
		{
			DataComparator.get().getSeenCommandRegular(sender, mplayer);
		}
	}
	
	@Override
	public List<String> getAliases()
	{
		return MUtil.list("seen");
	}
}
