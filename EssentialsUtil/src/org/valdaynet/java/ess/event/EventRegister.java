package org.valdaynet.java.ess.event;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.valdaynet.java.ess.EssentialUtil;
import org.valdaynet.java.ess.entity.MConf;
import org.valdaynet.java.ess.entity.MPlayer;
import org.valdaynet.java.ess.entity.MPlayerColl;

import com.dthielke.herochat.Chatter;
import com.dthielke.herochat.Herochat;
import com.massivecraft.massivecore.util.Txt;
import com.massivecraft.vampire.entity.UPlayer;

public class EventRegister implements Listener {
	
	private static EventRegister event = new EventRegister();
	public static EventRegister get() { return event; }
	
	public void listen()
	{
		Bukkit.getPluginManager().registerEvents(this, EssentialUtil.get());
	}
	
	@EventHandler
	public void onQuit(PlayerQuitEvent ev)
	{
		Player player = ev.getPlayer();
		MPlayer mpl = MPlayerColl.get().get(player);
		if (MConf.get().isVaultOverriden())
		{
			double amount = EssentialUtil.get().getEconomy().getBalance(player);
			mpl.setEconomyBalance(amount);
		}
		if (MConf.get().isFactionsCompliant())
		{
			com.massivecraft.factions.entity.MPlayer mplayer = com.massivecraft.factions.entity.MPlayer.get(player);
			String factionName = mplayer.getFaction().getName();
			mpl.setFaction(factionName);
		}
		if (MConf.get().isHerochatCompliant())
		{
			Chatter chat = Herochat.getChatterManager().getChatter(player);
			String channelName = chat.getActiveChannel().getName();
			mpl.setHerochatChannel(channelName);
		}
		if (MConf.get().isVampireCompliant())
		{
			UPlayer upl = UPlayer.get(player);
			boolean isInfected = upl.isInfected();
			boolean isVampire = upl.isVampire();
			mpl.setVampireInfected(isInfected);
			mpl.setVampireVampire(isVampire);
		}
	}
	@EventHandler
	public void onKick(PlayerKickEvent ev)
	{
		Player player = ev.getPlayer();
		MPlayer mpl = MPlayerColl.get().get(player);
		if (MConf.get().isVaultOverriden())
		{
			double amount = EssentialUtil.get().getEconomy().getBalance(player);
			mpl.setEconomyBalance(amount);
		}
		if (MConf.get().isFactionsCompliant())
		{
			com.massivecraft.factions.entity.MPlayer mplayer = com.massivecraft.factions.entity.MPlayer.get(player);
			String factionName = mplayer.getFaction().getName();
			mpl.setFaction(factionName);
		}
		if (MConf.get().isHerochatCompliant())
		{
			Chatter chat = Herochat.getChatterManager().getChatter(player);
			String channelName = chat.getActiveChannel().getName();
			mpl.setHerochatChannel(channelName);
		}
		if (MConf.get().isVampireCompliant())
		{
			UPlayer upl = UPlayer.get(player);
			boolean isInfected = upl.isInfected();
			boolean isVampire = upl.isVampire();
			mpl.setVampireInfected(isInfected);
			mpl.setVampireVampire(isVampire);
		}
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent ev)
	{
		MPlayer mplayer = MPlayer.get(ev.getPlayer());
		if (!mplayer.isBanned()) return;
		mplayer.getPlayer().kickPlayer(Txt.parse("<lime><strong>"+MConf.get().getServer()+" <i>has you listed as a banned player."));
	}
}
