package org.valdaynet.java.ess;

public enum Perm {
	
	SEEN("seen"),
	SEEN_ADMIN("admin.seen"),
	BAN_PLAYER("admin.ban"),
	PARDON_PLAYER("admin.pardon"),
	EVENT_REGISTER("events.register"),
	EVENT_SCHEDULE("events.schedule"),
	EVENT_DESC("events.description");
	
	private String node;
	
	private Perm(String node1)
	{
		this.node = "imperatives."+node;
	}
	
	public String getNode()
	{
		return node;
	}
}
