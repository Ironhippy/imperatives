package org.valdaynet.java.ess.entity;

import com.massivecraft.massivecore.store.Entity;

public class MEvent extends Entity<MEvent> {
	
	@Override
	public MEvent load(MEvent that)
	{
		return this;
	}
	
	private String yearScheduled = null;
	public void setYearScheduled(String year) { this.yearScheduled = year; this.changed(); }
	public String getYear() { return this.yearScheduled; }
	
	private String monthScheduled = null;
	public void setMonthScheduled(String month) { this.monthScheduled = month; this.changed(); }
	public String getMonth() { return this.monthScheduled; }
}
