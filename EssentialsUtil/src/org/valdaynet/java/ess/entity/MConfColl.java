package org.valdaynet.java.ess.entity;

import org.valdaynet.java.ess.EssentialUtil;

import com.massivecraft.massivecore.MassiveCore;
import com.massivecraft.massivecore.store.Coll;
import com.massivecraft.massivecore.store.MStore;

public class MConfColl extends Coll<MConf> {
	
	private static MConfColl i = new MConfColl();
	public static MConfColl get() { return i; }
	
	public MConfColl()
	{
		super("imperitives_conf", MConf.class, MStore.getDb(), EssentialUtil.get());
	}
	
	@Override
	public void onTick()
	{
		super.onTick();
	}
	
	@Override
	public void init()
	{
		super.init();
		
		MConf.i = this.get(MassiveCore.INSTANCE, true);
	}
}
