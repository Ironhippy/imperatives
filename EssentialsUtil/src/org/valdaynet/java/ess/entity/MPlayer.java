package org.valdaynet.java.ess.entity;

import org.bukkit.Bukkit;
import org.valdaynet.java.ess.EssentialUtil;

import com.dthielke.herochat.Chatter;
import com.dthielke.herochat.Herochat;
import com.massivecraft.massivecore.ps.PS;
import com.massivecraft.massivecore.store.SenderEntity;
import com.massivecraft.massivecore.util.Txt;
import com.massivecraft.vampire.entity.UPlayer;

public class MPlayer extends SenderEntity<MPlayer> {
	
	public static MPlayer get(Object oid)
	{
		return MPlayerColl.get().get(oid);
	}
	
	// +++++++++++++++ //
	// INITIALIZE DATA //
	// +++++++++++++++ //
	
	private String isStaff = Txt.parse("<silver><italic>No, this player is not a staff member.");
	private boolean isBanned = false;
	private String banMsg = Txt.parse("<i>This player has never broken our rules.");
	private PS playerLocation = PS.valueOf(Bukkit.getWorlds().get(0).getSpawnLocation());
	private boolean isInfected = false;
	private boolean isVampire = false;
	private String hasFaction = Txt.parse("<green>Wilderness");
	private double vaultEconomyBalance = 0;
	private String lastChannel = "General";
	private PS lyncValue = PS.valueOf(Bukkit.getWorlds().get(0).getSpawnLocation());
	
	@Override
	public MPlayer load(MPlayer that)
	{
		this.setStaff(that.isStaff);
		this.isBanned = that.isBanned;
		this.banMsg = that.banMsg;
		this.playerLocation = that.playerLocation;
		this.isInfected = that.isInfected;
		this.isVampire = that.isVampire;
		this.hasFaction = that.hasFaction;
		this.vaultEconomyBalance = that.vaultEconomyBalance;
		this.lastChannel = that.lastChannel;
		Bukkit.getConsoleSender().sendMessage(Txt.parse("<teal>[<aqua>Imperatives "+EssentialUtil.get().getDescription().getVersion()+"<teal>] <i>Configured "+this.getName()+"'s Settings."));
		
		return this;
	}
	
	
	// +++++++++++++++ //
	//  RETRIEVE DATA  //
	// +++++++++++++++ //
	
	
	//Is said player infected, though still human?
	public boolean isVampireInfected()
	{
		if (this.isOnline())
		{
			UPlayer upl = UPlayer.get(getPlayer());
			boolean iInfected = upl.isInfected();
			return iInfected;
		}
		return this.isInfected;
	}
	
	//Is said player a full-pledged vampire? P.S. Infection will return false if vampire returns true.
	public boolean isVampireVampire()
	{
		if (this.isOnline())
		{
			UPlayer upl = UPlayer.get(getPlayer());
			boolean iVampire = upl.isVampire();
			return iVampire;
		}
		return this.isVampire;
	}
	
	//What faction is said player a member of? Will display {SafeZone, WarZone, Wilderness}
	public String hasFactionFaction()
	{
		if (this.isOnline())
		{
			com.massivecraft.factions.entity.MPlayer mpl = com.massivecraft.factions.entity.MPlayer.get(getPlayer());
			return mpl.getFactionName();
		}
		return this.hasFaction;
	}
	
	//How much money does this player possess? Is he or she rich?
	public double getEconomyBalance()
	{
		if (this.isOnline())
		{
			double accountAmount = EssentialUtil.get().getEconomy().getBalance(getPlayer());
			return accountAmount;
		}
		return this.vaultEconomyBalance;
	}
	
	//Is said player in a chat channel?
	public String getHerochatChannel()
	{
		if (this.isOnline())
		{
			Chatter chat = Herochat.getChatterManager().getChatter(getPlayer());
			String channelName = chat.getName();
			return channelName;
		}
		return this.lastChannel;
	}
	
	public PS getLocation() { return this.playerLocation; }
	
	public PS getLyncLocation() { return this.lyncValue; }
	
	public String getBanMessage() { return this.banMsg; }
	public boolean isBanned() { return this.isBanned; }
	
	// ++++++++++++++ //
	// OVERWRITE DATA //
	// ++++++++++++++ //
	
	public void setStaff(String str)
	{
		this.isStaff = str;
		this.changed();
	}
	
	public void setVampireInfected(boolean bool)
	{
		this.isInfected = bool;
		this.changed();
	}
	
	public void setVampireVampire(boolean bool)
	{
		this.isVampire = bool;
		this.changed();
	}
	
	public void setFaction(String str)
	{
		this.hasFaction = str;
		this.changed();
	}
	
	public void setEconomyBalance(double amount)
	{
		this.vaultEconomyBalance = amount;
		this.changed();
	}
	
	public void setHerochatChannel(String channelName)
	{
		this.lastChannel = channelName;
		this.changed();
	}
	
	public void setPS(PS ps)
	{
		this.playerLocation = ps;
		this.changed();
	}
	
	public void setBanned(boolean bool, String reason)
	{
		this.isBanned = bool;
		this.banMsg = reason;
		this.changed();
	}
	
	public void setLync(PS ps)
	{
		this.lyncValue = ps;
		this.changed();
	}
}
