package org.valdaynet.java.ess.entity;

import java.util.ArrayList;
import java.util.List;

import org.valdaynet.java.ess.EssentialUtil;

import com.massivecraft.massivecore.store.MStore;
import com.massivecraft.massivecore.store.SenderColl;

public class MPlayerColl extends SenderColl<MPlayer> {
	
	private static MPlayerColl i = new MPlayerColl();
	public static MPlayerColl get() { return i; }

	public MPlayerColl() {
		super("imperatives_mplayer", MPlayer.class, MStore.getDb(), EssentialUtil.get());
	}
	
	@Override
	public void onTick()
	{
		super.onTick();
	}
	
	public MPlayer getByName(String str)
	{
		MPlayer mplayer = null;
		for (MPlayer mpl : this.getAll())
		{
			if (mpl.getName().equalsIgnoreCase(str))
			{
				mplayer = mpl;
				break;
			}
		}
		return mplayer;
	}
	
	public List<String> getMPlayerNames()
	{
		List<String> names = new ArrayList<String>();
		for (MPlayer mpl : this.getAll())
		{
			names.add(mpl.getName());
		}
		return names;
	}
}
