package org.valdaynet.java.ess.entity;

import com.massivecraft.massivecore.store.Entity;
import com.massivecraft.massivecore.util.Txt;

public class MConf extends Entity<MConf> {
	
	protected static transient MConf i;
	public static MConf get() { return i; }
	
	private String serverName = Txt.parse("GrayStone");
	private boolean isFactionsCompliant = false;
	private boolean isVaultOverriden = false;
	private boolean isHerochatCompliant = false;
	private boolean isVampireCompliant = false;
	private boolean seeEconomyCheck = true;
	private boolean seeWorldCheck = true;
	private boolean seeNickCheck = true;
	private boolean seeFactionCheck = true;
	private boolean seeVampireCheck = true;
	private boolean seeInfectedCheck = true;
	private boolean seeChannelCheck = false;
	
	@Override
	public MConf load(MConf that)
	{
		super.load(that);
		
		this.isFactionsCompliant = false;
		this.isVaultOverriden = false;
		this.isHerochatCompliant = false;
		this.isVampireCompliant = false;
		
		return this;
	}
	
	// +++++++++++++++ //
	// LOADING METHODS //
	// +++++++++++++++ //
	
	public void setFactionsCompliant()
	{
		this.isFactionsCompliant = true;
	}
	
	public void setVaultCompliant()
	{
		this.isVaultOverriden = true;
	}
	
	public void setHerochatCompliant()
	{
		this.isHerochatCompliant = true;
	}
	
	public void setVampireCompliant()
	{
		this.isVampireCompliant = true;
	}
	// +++++++++++++++ //
	// ENABLED METHODS //
	// +++++++++++++++ //
	
	public boolean isFactionsCompliant()
	{
		return this.isFactionsCompliant;
	}
	
	public boolean isVaultOverriden()
	{
		return this.isVaultOverriden;
	}
	
	public boolean isHerochatCompliant()
	{
		return this.isHerochatCompliant;
	}
	
	public boolean isVampireCompliant()
	{
		return this.isVampireCompliant;
	}
	
	// ++++++++++++++++++++ //
	// SEEN COMMAND METHODS //
	// ++++++++++++++++++++ //
	
	public boolean canSeeWorld()
	{
		return this.seeWorldCheck;
	}
	
	public boolean canSeeNick()
	{
		return this.seeNickCheck;
	}
	
	public boolean canSeeMoney()
	{
		return this.seeEconomyCheck;
	}
	
	public boolean canSeeFaction()
	{
		return this.seeFactionCheck;
	}
	
	public boolean canSeeInfected()
	{
		return this.seeInfectedCheck;
	}
	
	public boolean canSeeVampire()
	{
		return this.seeVampireCheck;
	}
	
	public boolean canSeeChannel()
	{
		return this.seeChannelCheck;
	}
	public String getServer()
	{
		return this.serverName;
	}
}
